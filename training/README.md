# README #

### Setup
The Jupyter notebook are all for the model training. It is recommended to use anaconda to create a python environment. It is important to use Python 3.6, because the glove training model is not working with Python 3.7.

After creating the python environment, run 
`pip install -r requirements.txt`
to install all required libraries.


### data

Before you download the data, please make a data folder in the same direction of notebooks files.

You could download data from this google drive link: 

https://drive.google.com/drive/folders/1qp1hcweqMmYFEVC6RvIepRKBWVt4pH2d?usp=sharing


Additional data:
* You need download NLTK and Glove pre-trained model to make some code working properly.

### Chrome-extension code
We did partial chrome extension for this project. You could download from this google drive link.
https://drive.google.com/drive/folders/1vCBPx2PdcjLHqRwtTOGJeMpGtB4HWiUR?usp=sharing


### Additional
All code and file are also stored in bitbucket

https://bitbucket.org/JunchengHan/csc690-project/src/master/
